#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QTextStream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>

#include <QSerialPort>
#include <QSerialPortInfo>

#define QT_THREAD_SUPPORT
#include <qmutex.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->plot->addGraph();



    QTimer *timer = new QTimer(this);
    timer->start(50);
//    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(update_plot()));




}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update_plot()
{
    QTime time_ = QTime::currentTime();
    QString text = time_.toString("mm:ss");
    ui->clock_view->display(text);


    QVector<double> x(100), y(100);
    srand (clock());

    for (int i=0; i<100; ++i)
    {
      x[i] = i; // x goes from -1 to 1
      y[i] = rand()%100; // let's plot a quadratic function
    }



    ui->plot->graph(0)->setData(x, y);
    ui->plot->graph(0)->rescaleAxes();
    ui->plot->replot();


}

void MainWindow::startSlave(const QString &portName, int waitTimeout, const QString &response)
{
    const QMutexLocker locker(&m_mutex);
    m_portName = portName;
    m_waitTimeout = waitTimeout;
    m_response = response;
    if (!isRunning())
        start();
}


void MainWindow::addPoint(double x, double y)
{
   QTextStream(stdout) << y << endl;
   ui->plot->graph(0)->addData(x, y);
   ui->plot->replot();
}


void MainWindow::plot()
{
//    ui->plot->graph(0)->setData(qv_x, qv_y);

}


void MainWindow::on_DialFrec_rangeChanged(int min, int max)
{

}

void MainWindow::on_DialFrec_sliderMoved(int position)
{
    ui->LCDFrec->display(position);
}



void MainWindow::on_DialGain_sliderMoved(int position)
{
    ui->LCDGain->display(position);
}


