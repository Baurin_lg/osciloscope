#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addPoint(double x, double y);
    void plot();
    void timer_event();



    void startSlave(const QString &portName, int waitTimeout, const QString &response);


private slots:
    void on_DialFrec_rangeChanged(int min, int max);

    void on_DialFrec_sliderMoved(int position);

    void on_DialGain_sliderMoved(int position);

private:
    Ui::MainWindow *ui;
    void run() override;

    QString m_portName;
    QString m_response;
    int m_waitTimeout = 0;
    QMutex m_mutex;
    bool m_quit = false;

public slots:
    void update_plot();

};

#endif // MAINWINDOW_H
